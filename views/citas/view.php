<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Citas */

$this->title = $model->id_mascota;
$this->params['breadcrumbs'][] = ['label' => 'Citas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="citas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_mascota' => $model->id_mascota, 'cod_mascotas' => $model->cod_mascotas], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_mascota' => $model->id_mascota, 'cod_mascotas' => $model->cod_mascotas], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_mascota',
            'cod_mascotas',
            'fecha_cita',
            'hora_cita',
            'descripcion',
        ],
    ]) ?>

</div>
