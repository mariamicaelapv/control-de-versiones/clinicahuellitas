<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vacunas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacunas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'n_dosis')->textInput() ?>

    <?= $form->field($model, 'enfermedad_trata')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_aplicacion')->textInput() ?>

    <?= $form->field($model, 'cod_mascotas')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
