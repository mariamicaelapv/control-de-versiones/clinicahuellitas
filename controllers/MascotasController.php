<?php

namespace app\controllers;

use Yii;
use app\models\Mascotas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MascotasController implements the CRUD actions for Mascotas model.
 */
class MascotasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mascotas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Mascotas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mascotas model.
     * @param string $cod
     * @param string $cod_cliente
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod, $cod_cliente)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod, $cod_cliente),
        ]);
    }

    /**
     * Creates a new Mascotas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mascotas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod' => $model->cod, 'cod_cliente' => $model->cod_cliente]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Mascotas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $cod
     * @param string $cod_cliente
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod, $cod_cliente)
    {
        $model = $this->findModel($cod, $cod_cliente);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod' => $model->cod, 'cod_cliente' => $model->cod_cliente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Mascotas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $cod
     * @param string $cod_cliente
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod, $cod_cliente)
    {
        $this->findModel($cod, $cod_cliente)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mascotas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $cod
     * @param string $cod_cliente
     * @return Mascotas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod, $cod_cliente)
    {
        if (($model = Mascotas::findOne(['cod' => $cod, 'cod_cliente' => $cod_cliente])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
