<?php

namespace app\controllers;

use Yii;
use app\models\Citas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CitasController implements the CRUD actions for Citas model.
 */
class CitasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Citas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Citas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Citas model.
     * @param string $id_mascota
     * @param string $cod_mascotas
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_mascota, $cod_mascotas)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_mascota, $cod_mascotas),
        ]);
    }

    /**
     * Creates a new Citas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Citas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_mascota' => $model->id_mascota, 'cod_mascotas' => $model->cod_mascotas]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Citas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id_mascota
     * @param string $cod_mascotas
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_mascota, $cod_mascotas)
    {
        $model = $this->findModel($id_mascota, $cod_mascotas);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_mascota' => $model->id_mascota, 'cod_mascotas' => $model->cod_mascotas]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Citas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id_mascota
     * @param string $cod_mascotas
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_mascota, $cod_mascotas)
    {
        $this->findModel($id_mascota, $cod_mascotas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Citas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id_mascota
     * @param string $cod_mascotas
     * @return Citas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_mascota, $cod_mascotas)
    {
        if (($model = Citas::findOne(['id_mascota' => $id_mascota, 'cod_mascotas' => $cod_mascotas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
