<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "veterinarios".
 *
 * @property string $cod
 * @property string|null $DNI
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $hora_entrada
 * @property string|null $hora_salida
 *
 * @property Clientes[] $clientes
 * @property Telefonos[] $telefonos
 */
class Veterinarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['hora_entrada', 'hora_salida'], 'safe'],
            [['cod'], 'string', 'max' => 10],
            [['DNI'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 15],
            [['apellidos'], 'string', 'max' => 20],
            [['cod'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'DNI' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'hora_entrada' => 'Hora Entrada',
            'hora_salida' => 'Hora Salida',
        ];
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Clientes::className(), ['cod_vet' => 'cod']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['cod_vet' => 'cod']);
    }
}
