<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property string $cod
 * @property string|null $nombre
 * @property string|null $tipo_producto
 * @property int|null $precio
 * @property string|null $marca
 * @property string|null $cod_cliente
 *
 * @property Clientes $codCliente
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['precio'], 'integer'],
            [['cod', 'cod_cliente'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 15],
            [['tipo_producto'], 'string', 'max' => 30],
            [['marca'], 'string', 'max' => 50],
            [['cod'], 'unique'],
            [['cod_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cod_cliente' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'tipo_producto' => 'Tipo Producto',
            'precio' => 'Precio',
            'marca' => 'Marca',
            'cod_cliente' => 'Cod Cliente',
        ];
    }

    /**
     * Gets query for [[CodCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCliente()
    {
        return $this->hasOne(Clientes::className(), ['cod' => 'cod_cliente']);
    }
}
