<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citas".
 *
 * @property string $id_mascota
 * @property string $cod_mascotas
 * @property string|null $fecha_cita
 * @property string|null $hora_cita
 * @property string|null $descripcion
 *
 * @property Mascotas $codMascotas
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mascota', 'cod_mascotas'], 'required'],
            [['fecha_cita', 'hora_cita'], 'safe'],
            [['id_mascota', 'cod_mascotas'], 'string', 'max' => 10],
            [['descripcion'], 'string', 'max' => 150],
            [['id_mascota', 'cod_mascotas'], 'unique', 'targetAttribute' => ['id_mascota', 'cod_mascotas']],
            [['cod_mascotas'], 'exist', 'skipOnError' => true, 'targetClass' => Mascotas::className(), 'targetAttribute' => ['cod_mascotas' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mascota' => 'Id Mascota',
            'cod_mascotas' => 'Cod Mascotas',
            'fecha_cita' => 'Fecha Cita',
            'hora_cita' => 'Hora Cita',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[CodMascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodMascotas()
    {
        return $this->hasOne(Mascotas::className(), ['cod' => 'cod_mascotas']);
    }
}
