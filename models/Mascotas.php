<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mascotas".
 *
 * @property string $cod
 * @property string $cod_cliente
 * @property string|null $nombre
 * @property string|null $sexo
 * @property string|null $nombre_veterinario
 * @property string|null $raza
 *
 * @property Citas[] $citas
 * @property Clientes $codCliente
 * @property Vacunas[] $vacunas
 */
class Mascotas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mascotas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod', 'cod_cliente'], 'required'],
            [['cod', 'cod_cliente', 'sexo'], 'string', 'max' => 10],
            [['nombre', 'nombre_veterinario'], 'string', 'max' => 15],
            [['raza'], 'string', 'max' => 25],
            [['cod', 'cod_cliente'], 'unique', 'targetAttribute' => ['cod', 'cod_cliente']],
            [['cod_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cod_cliente' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'cod_cliente' => 'Cod Cliente',
            'nombre' => 'Nombre',
            'sexo' => 'Sexo',
            'nombre_veterinario' => 'Nombre Veterinario',
            'raza' => 'Raza',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['cod_mascotas' => 'cod']);
    }

    /**
     * Gets query for [[CodCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCliente()
    {
        return $this->hasOne(Clientes::className(), ['cod' => 'cod_cliente']);
    }

    /**
     * Gets query for [[Vacunas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVacunas()
    {
        return $this->hasMany(Vacunas::className(), ['cod_mascotas' => 'cod']);
    }
}
