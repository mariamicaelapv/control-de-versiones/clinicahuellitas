<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property string $cod_vet
 * @property string $telefono
 *
 * @property Veterinarios $codVet
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_vet', 'telefono'], 'required'],
            [['cod_vet'], 'string', 'max' => 10],
            [['telefono'], 'string', 'max' => 20],
            [['cod_vet', 'telefono'], 'unique', 'targetAttribute' => ['cod_vet', 'telefono']],
            [['cod_vet'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinarios::className(), 'targetAttribute' => ['cod_vet' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_vet' => 'Cod Vet',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[CodVet]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodVet()
    {
        return $this->hasOne(Veterinarios::className(), ['cod' => 'cod_vet']);
    }
}
