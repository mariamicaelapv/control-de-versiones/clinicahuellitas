<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacunas".
 *
 * @property string $cod
 * @property string|null $nombre
 * @property int|null $n_dosis
 * @property string|null $enfermedad_trata
 * @property string|null $fecha_aplicacion
 * @property string|null $cod_mascotas
 *
 * @property Mascotas $codMascotas
 */
class Vacunas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacunas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['n_dosis'], 'integer'],
            [['fecha_aplicacion'], 'safe'],
            [['cod', 'cod_mascotas'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 15],
            [['enfermedad_trata'], 'string', 'max' => 50],
            [['cod'], 'unique'],
            [['cod_mascotas'], 'exist', 'skipOnError' => true, 'targetClass' => Mascotas::className(), 'targetAttribute' => ['cod_mascotas' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'n_dosis' => 'N Dosis',
            'enfermedad_trata' => 'Enfermedad Trata',
            'fecha_aplicacion' => 'Fecha Aplicacion',
            'cod_mascotas' => 'Cod Mascotas',
        ];
    }

    /**
     * Gets query for [[CodMascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodMascotas()
    {
        return $this->hasOne(Mascotas::className(), ['cod' => 'cod_mascotas']);
    }
}
