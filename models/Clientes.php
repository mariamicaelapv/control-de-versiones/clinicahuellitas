<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $cod
 * @property string|null $telefono
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $cod_vet
 *
 * @property Veterinarios $codVet
 * @property Mascotas[] $mascotas
 * @property Productos[] $productos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['cod', 'cod_vet'], 'string', 'max' => 10],
            [['telefono', 'apellidos'], 'string', 'max' => 20],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 15],
            [['cod'], 'unique'],
            [['cod_vet'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinarios::className(), 'targetAttribute' => ['cod_vet' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'telefono' => 'Telefono',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'cod_vet' => 'Cod Vet',
        ];
    }

    /**
     * Gets query for [[CodVet]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodVet()
    {
        return $this->hasOne(Veterinarios::className(), ['cod' => 'cod_vet']);
    }

    /**
     * Gets query for [[Mascotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMascotas()
    {
        return $this->hasMany(Mascotas::className(), ['cod_cliente' => 'cod']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['cod_cliente' => 'cod']);
    }
}
